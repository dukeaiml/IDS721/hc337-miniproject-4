# Axum Todo Site [![Pipeline Status](https://gitlab.com/dukeaiml/IDS721/hc337-miniproject-4/badges/master/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/hc337-miniproject-4/-/pipelines)

## Table of Contents

1. [Description](#description)
2. [Installation](#installation)
3. [Usage](#usage)
4. [Contributing](#contributing)
5. [Development Process](#development-process)

## Description

This project is a simple web application built with Rust and Axum. It is a todo list application that allows users to create, update, and delete tasks.

## Installation

To install this project, you will need to have Rust and Docker installed on your machine. Once you have these dependencies, you can clone the repository and run the following commands:

```
$ make build
```

## Usage

To use this project, you can run the following command:

```
$ make rundocker
```

![Run Docker Example](/screenshots/run_docker.png)

This will start the web application and you can access it in your browser at `http://localhost:3000`.

![Site Screenshot](/screenshots/site_filled_screenshot.png)

## Contributing

If you would like to contribute to this project, please fork the repository and submit a pull request. We welcome all contributions.

## Development Process

The development of this project involved several key steps:

-   Designing the basic structure of the application, initalizing the structure for the `axum` backend.
-   Implementing the backend logic. This involved creating the necessary routes, handlers, and data structures.
-   Adding a front-end. We decided to leverage the `minijinja` templating library to create a user-accessible interface
-   Fleshing out documentation for the project and making it portable by packing a Dockerfile
