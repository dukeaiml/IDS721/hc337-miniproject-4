use axum::{
    extract::{Path, State},
    http::StatusCode,
    response::{Html, IntoResponse},
    Json,
};
use minijinja::{context, Environment};
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    io::Error,
    sync::{Arc, Mutex},
    time::Instant,
};
use uuid::Uuid;

#[derive(Debug, Serialize, Clone)]
pub struct Todo {
    pub id: Uuid,
    #[serde(skip_serializing)]
    pub creation_time: Instant,
    pub name: String,
    pub finished: bool,
}

#[derive(Debug, Deserialize)]
pub struct CreateTodo {
    name: String,
}

#[derive(Debug, Deserialize)]
pub struct UpdateTodo {
    name: Option<String>,
    finished: Option<bool>,
}

#[derive(Default, Clone)]
pub struct Db {
    data: Arc<Mutex<HashMap<Uuid, Todo>>>,
}

impl Db {
    pub fn new() -> Self {
        Db {
            data: Arc::new(Mutex::new(HashMap::new())),
        }
    }
    pub fn read(&self) -> Result<Vec<Todo>, Error> {
        let todos = self
            .data
            .lock()
            .unwrap()
            .iter()
            .map(|(_, t)| t)
            .cloned()
            .collect();
        Ok(todos)
    }
    pub fn get_todo(&self, id: Uuid) -> Result<Todo, String> {
        let inner = self.data.lock().unwrap();
        inner
            .get(&id)
            .cloned()
            .ok_or_else(|| "Unable to find todo".into())
    }
    pub fn add_todo(&self, todo: Todo) -> Result<(), Error> {
        let mut inner = self.data.lock().unwrap();
        inner.insert(todo.id, todo.clone());
        Ok(())
    }
    pub fn delete_todo(&self, id: Uuid) -> Result<(), Error> {
        let mut inner = self.data.lock().unwrap();
        inner.remove(&id);
        Ok(())
    }
}

pub async fn list_todos(State(db): State<Db>) -> Result<impl IntoResponse, StatusCode> {
    let mut todos = db.read().map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    todos.sort_by(|a, b| a.creation_time.cmp(&b.creation_time));
    Ok(Json(todos))
}

pub async fn create_todo(
    State(db): State<Db>,
    Json(input): Json<CreateTodo>,
) -> Result<impl IntoResponse, StatusCode> {
    let id = Uuid::new_v4();
    let todo = Todo {
        id: id,
        creation_time: Instant::now(),
        name: input.name,
        finished: false,
    };
    db.add_todo(todo.clone())
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    Ok((StatusCode::CREATED, Json(todo)))
}

pub async fn update_todo(
    State(db): State<Db>,
    Path(id): Path<Uuid>,
    Json(input): Json<UpdateTodo>,
) -> Result<impl IntoResponse, StatusCode> {
    println!("{:?}", input);
    let mut todo = db.get_todo(id).map_err(|_| StatusCode::NOT_FOUND)?;
    if let Some(name) = input.name {
        todo.name = name;
    }
    if let Some(finished) = input.finished {
        todo.finished = finished;
    }
    println!("{:?}", todo);
    db.add_todo(todo.clone())
        .map(|_| Json(todo))
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)
}

pub async fn delete_todo(
    State(db): State<Db>,
    Path(id): Path<Uuid>,
) -> Result<impl IntoResponse, StatusCode> {
    db.delete_todo(id).map_err(|_| StatusCode::NOT_FOUND)
}

static ENV: Lazy<Environment<'static>> = Lazy::new(|| {
    let mut env = Environment::new();
    minijinja_embed::load_templates!(&mut env);
    env
});

pub async fn root(State(db): State<Db>) -> impl IntoResponse {
    let template = ENV.get_template("home.jinja").unwrap();
    let ctx = context!(todos => db.read().unwrap());
    Html(template.render(ctx).unwrap())
}
