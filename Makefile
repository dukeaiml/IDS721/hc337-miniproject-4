install:
	cargo clean &&\
		cargo build -j 1

build:
	docker-compose build

rundocker:
	docker-compose up

format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

run:
	cargo run

